package main

import (
	"fmt"
	"sync"
)

var mutex sync.Mutex

const bufferSize = 1000
const maximumSubscriber = 100

func main() {
	var mq messageQueue
	mq = initChanelQueue()

	wg := new(sync.WaitGroup)

	for i := 0; i < 10; i++ {
		wg.Add(1)
		go func(index int) {
			defer wg.Done()
			scTest1 := mq.Subscribe("test1")
			scTest2 := mq.Subscribe("test2")
			for true {
				select {
				case message := <-scTest1:
					fmt.Printf("#%d: message received from test1 %s\n", index, message)
				case message := <-scTest2:
					fmt.Printf("#%d: message received from test2 %s\n", index, message)

				}
			}
		}(i)
	}

	for true {
		var topic, message string
		fmt.Scanf("%s\n", &topic)
		fmt.Scanf("%s\n", &message)
		mq.Publish(topic, message)
	}
	wg.Wait()
}

type messageQueue interface {
	Subscribe(topic string) chan string
	Publish(topic, message string)
}

type chanelQueue struct {
	Msc map[string][]chan string
}

func (cq chanelQueue) Subscribe(topic string) chan string {
	mutex.Lock()
	defer mutex.Unlock()

	cList := cq.Msc[topic]
	cList = append(cList, make(chan string, bufferSize))
	cq.Msc[topic] = cList
	return cList[len(cList)-1]
}

func (cq chanelQueue) Publish(topic, message string) {
	fmt.Printf("going to publish %s on %s\n", message, topic)
	subscribedList := cq.Msc[topic]
	for i, subscribedChanel := range subscribedList {
		fmt.Printf("publsish to %d\n", i)
		subscribedChanel <- message
	}
}

func initChanelQueue() chanelQueue {
	ch := make(map[string][]chan string, maximumSubscriber)
	return chanelQueue{Msc: ch}
}
